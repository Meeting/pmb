        <!--LISTADO DE REPORTES-->
        <div class="reportes sub report-list user-profile" style="visibility:hidden">
            <!--Intro reports-->
            <div class="ir" id="top-profile">
                <div class="c-t profile">
                	<a href="javascript:;" class="btn btn-sky btn-small btn-comentar btn-editar">Editar</a>
                    <div class="img-profile">
                    	<img class="img-circle" src="images/user-profile-ref.png" alt="Roberto Carlo" />                    	
                    </div>                    
                    <h2>Roberto Carlo</h2>
                    <ul>
                    	<li>Periodista</li>
                        <li class="dot"><a href="javascript:;">La Blanquedada</a></li>
                    </ul>
                    <p>Amo el barrio, los vecinos y la tranquilidad. Siempre que puedo trato de aportar mi granito de arena. 
                    Hagamos de Montevideo un mejor lugar!</p>
                </div>
            </div>
            <!--/end intro reports-->
            <!--REPORTS LIST-->
            <div class="c-scroll-user">
                <div class="rl scrolled">                    	
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-arbolado"></div>
                        <div class="it-r-info">
                            <h3 class="arbolado">Arbolado</h3>
                            <p>Árboles o ramas caídas: accidental</p>
                            <ul class="details">
                                <li>Hoy, 16:00hs</li>
                                <li class="icon-camera"><span></span></li>
                                <li class="icon-comments"><span></span></li>
                                <li class="icon-tags"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-alcantarillas"></div>
                        <div class="it-r-info">
                            <h3 class="alcantarillas">alcantarillas y registros</h3>
                            <p>Alcantarilla obstruida</p>
                            <ul class="details">
                                <li>Hoy, 19:00hs</li>
                                <li class="icon-camera"><span></span></li>
                                <li class="icon-comments"><span></span></li>
                                <li class="icon-tags"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-basura"></div>
                        <div class="it-r-info">
                            <h3 class="basura">Basura, Escombros y ferias</h3>
                            <p>Levante de bolsas de barrido</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-desarrollo"></div>
                        <div class="it-r-info">
                            <h3 class="desarrollo">Desarrollo</h3>
                            <p>Problema de desarrollo 2</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-barometrica"></div>
                        <div class="it-r-info">
                            <h3 class="barometrica">Barometrica</h3>
                            <p>Solicitud de barométrica</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Reporte final</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                </div>
                <!--/end reports list-->
            </div>
        </div>
        <!--end listado de reportes-->
