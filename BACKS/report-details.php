        <!--DETALLE DE REPORTE-->
        <div class="c-report">
            <div class="container-report">
                <!--REPORT-->
                <div class="report">
                    <div class="info-report scrolled-report">
                        <div class="image-report">
                            <a href="javascript:;" class="btn btn-close-report">Cerrar</a>
                            <img class="img-top" src="images/ref-hotel.jpg" alt=""/>
                        </div>
                        <!--Intro Report-->
                        <div class="intro-report">
                            <a href="javascript:;" class="btn btn-close-report">Cerrar</a>
                            <h3>Alcantarilla obstruida</h3>
                            <address>Bv. Gral. Artigas 1234 esq Eduardo Victor Haedo</address>
                            <p>
                            Donec quis turpis et elit vehicula dignissim ut quis lectus. Aenean gravida nisi vitae enim dignissim 
                            imperdiet. Ut eu viverra odio. Integer commodo massa mi, sit amet 
                            rutrum nunc vestibulum at. Integer viverra rhoncus mauris id laoreet. 
                            </p>
                            <div class="sign">
                       			<div class="picture">
                                	<img class="img-circle" src="images/user-ref.jpg" width="35" height="35" alt="Roberto Ricci">
                                </div>
                                <div class="author">
                            		<span>Publicado por <a href="javascript:;">Roberto Ricci</a>, ayer a las 19:00hs</span>
                                </div>
                            </div>
                        </div>
                        <!--Status Report-->
                        <div class="status-report">
                            <h4>Estado del reporte</h4>
                            <ul>
                                <li>
                                    Cuadrilla asignada para el 16/05/2014
                                    <span class="time-report">hace 1 hr</span>
                                </li>
                                <li>
                                    Problema ingresado al sistema
                                    <span class="time-report">hace 4 días</span>
                                </li>
                            </ul>
                        </div>
                        <!--Comments Report-->
                        <div class="comment-reports">
                            <h3>Comentarios</h3>
                            <div class="comment">
                                <div class="user-avatar"><img src="images/user-2.png" alt=""/></div>
                                <div class="user-info">
                                    <h4>Zlatan Ibrahimovic</h4>
                                    <span class="time-report">hace 2 días</span>
                                </div>
                                <div class="user-comment">
                                    <p>
                                        "You remember when your dad taught you to swim?" The boy nodded. "Well, in the University of 
                                        California” - that is the name we had for the houses - we taught young men and women 
                                        how to think, just as I have taught you now, by sand and pebbles and shells, to 
                                        know how many 
                                    </p>
                                </div>                            
                            </div>
                            <div class="comment imm-comment">
                                <div class="user-avatar"><img src="images/user-imm.png" alt=""/></div>
                                <div class="user-info">
                                    <h4>Intendencia de Montevideo</h4>
                                    <span class="time-report">hace 4 días</span>
                                </div>
                                <div class="user-comment">
                                    <p>
                                        La cuadrilla #12345 fue asignada para resolver este problema para el 16/05. Cualquier consulta...
                                    </p>
                                </div>                            
                            </div>
                            <div class="comment">
                                <div class="user-avatar"><img src="images/user-3.png" alt=""/></div>
                                <div class="user-info">
                                    <h4>Olivia Wilde</h4>
                                    <span class="time-report">hace 2 meses</span>
                                </div>
                                <div class="user-comment">
                                    <p>
                                        Ok, I saw this yesterday.
                                    </p>
                                </div> 
                                <div class="user-picture">
                                    <img class="img-responsive" src="images/ref-hotel.jpg" alt=""/>
                                </div>                           
                            </div>
                            <div class="comment imm-comment">
                                <div class="user-avatar"><img src="images/user-imm.png" alt=""/></div>
                                <div class="user-info">
                                    <h4>Intendencia de Montevideo</h4>
                                    <span class="time-report">hace 4 días</span>
                                </div>
                                <div class="user-comment">
                                    <p>
                                        La cuadrilla #12345 fue asignada para resolver este problema para el 16/05. Cualquier consulta...
                                    </p>
                                </div>                            
                            </div>
                            <!--INICIAR SESION-->
                            <div class="login">
                                <div class="bloque-sesion new-report">
                                    <div class="ir">
                                        <h2>Accede a tu cuenta</h2>
                                        <p>Conectate a tu cuenta para poder finalizar tu reporte y enviarlo a la Intendencia.</p>
                                    </div>
                                    <div class="social-buttons">
                                        <button class="btn btn-block btn-social btn-facebook">Registrate con Facebook</button>
                                        <button class="btn btn-block btn-social btn-twitter">Registrate con Twitter</button>
                                        <p>...ó ingresa los datos si ya tienes cuenta:</p>
                                    </div>
                                    <form role="form">
                                        <div class="form-group">
                                            <input class="form-control error" type="text" placeholder="Correo electrónico">                            	
                                            <input class="form-control" type="password" placeholder="Contraseña">                            	
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-block btn-reportar">Iniciar sesión</button>
                                        </div>
                                        <p class="no-account">¿No utilizas redes sociales? No hay problema!
                                            <br />                        
                                            <a class="green registrate" href="javascript:;">Regístrate aquí con tu mail</a>
                                        </p>
                                        <!--<div class="form-group">
                                            <a href="javascript:;" class="btn btn-block btn-green registrate">Regístrate aquí</a>
                                        </div>-->
                                    </form>
                                </div>
                                <!--end iniciar sesion-->
                                <!--LOGIN-->
                                <div class="bloque-registro new-report">
                                    <div class="ir">
                                        <h2>Regístrate y se parte</h2>
                                        <p>Crea una cuenta en PorMiBarrio y comienza a colaborar.</p>
                                    </div>
                                    <form role="form">
                                        <div class="form-group">
                                            <input class="form-control" type="text" placeholder="Nombre y apellido">                            	
                                            <input class="form-control" type="text" placeholder="Cédula de Identidad">                            	
                                            <input class="form-control" type="email" placeholder="Correo Electrónico" >
                                            <input class="form-control" type="password" placeholder="Contraseña" >
                                        </div> 
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox"> Al continuar con tu registro, aceptas haber leído y aceptado nuestra 
                                              <a href="javascript:;"> Política de privacidad</a>.
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-block btn-green">Regístrate</button>
                                        </div>
                                      </form>
                                </div>
                            </div>
                            </div>                
                            <!--end login-->
							<!--User comments here-->
                            <div class="comment comment-user">
                                <div class="user-avatar"><img src="images/user-1.png" alt=""/></div>
                                <div class="user-info comments-user-info">
                                    <!--nuevo-->
                                    <form role="form">
                                        <div class="form-group">
                                        	<input class="form-control" placeholder="Nombre" />
                                        	<input class="form-control" placeholder="Email" />
                                            <input type="file" id="userInputFile">
                                            <div class="boxs">
                                                <div class="checkbox">
                                                    <label>
                                                      <input type="checkbox">Marcar este problema como resuelto.                               
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                      <input type="checkbox">Mostrar nombre público.                               
                                                    </label>
                                                </div>
                                            </div>
                                            <textarea rows="" placeholder="Escriba aquí su comentario"></textarea>
                                            <button type="submit" class="btn btn-sky btn-small btn-comentar btn-disabled" disabled="disabled">Comentar</button>
                                            <!--Cuando el usuario inicia sesion, remover la clase btn-disabled-->
                                            <!--<a href="javascript:;" class="btn btn-photo"></a>-->
                                            <div class="clearfix"></div>
                                            <div class="actions">
                                                <ul class="actions">
                                                    <li><a href="javascript:;" class="follow-report">Seguir de cerca este reporte</a></li>
                                                    <li><a href="javascript:;" class="reportar-abuso">Reportar abuso</a></li>
                                                </ul>
                                                <div class="share">
                                                    <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                                    <div class="addthis_sharing_toolbox"></div>                            
                                                </div>
                                            </div>
                                        </div>                                        
                                    </form>
                                </div>                            
                            </div>
                        </div>
                    </div>
                </div>
                <!--end report-->
            </div>
            <!--end container report-->   
        </div>
        <!--end detalle de reporte report-->       
