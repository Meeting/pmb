<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Por mi Barrio</title>

    <!--CSS-->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/main.css" rel="stylesheet" />
    
    <!--FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body>
	<!--MAIN CONTAINER-->
    <div class="container">
    	<!--FIRST LEVEL NAVIGATION-->
        <nav class="first-navigation">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img class="img-responsive" src="images/bg/PMB-small.png" alt=""></a>
            <ul class="r-small">
                <li class="reportar reportar-small"><a href="reportar.php"><span>Reportar</span></a></li>
            </ul>
            <div class="navbar-collapse collapse">
                <ul>
                    <li class="home"><a href="index.php"><span></span></a></li>
                    <li class="reportar"><a href="reportar.php"><span>Reportar</span></a></li>
                    <li class="reportes"><a href="lista-reportes.php">Reportes</a></li>
                    <li class="help"><a href="info-help.php">Ayuda</a></li>
                    <li class="profile"><a href="profile.php"><img class="img-circle" width="35" height="35" src="images/user-profile-ref.png"/><span>Perfil</span></a></li>
                </ul>
                <ul class="help-info">
                    <li class="doc-info"><a href="javascript:;">Doc info</a></li>
                    <li class="data"><a href="javascript:;">DATA</a></li>
                    <li class="my-society"><a href="javascript:;">My Society</a></li>
                </ul>
            </div>
        </nav>
        
        
        <!--REPORT LIST-->
 <div class="reportes sub report-list" id="report-list">
            <!--Intro reports-->
            <div class="ir" id="top-reports">
                <div class="c-t"><!--nuevo-->
                    <h2>Todos los reportes</h2>
                    <p>Donec quis turpis et elit vehicula dignissim ut quis lectus. Aenean gravida nisi vitae enim dignissim imperdiet. 
                    Duis varius justo purus, et lobortis turpis sagittis sed.</p>
                </div>
            </div>
            <!--/end intro reports-->
            <!--REPORTS LIST-->
            <div class="c-scroll"><!--nuevo-->
                <div class="rl scrolled">                    	
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-arbolado"></div>
                        <a href="report-detail.php">
                        <div class="it-r-info">
                            <h3 class="arbolado">Arbolado</h3>
                            <p>Árboles o ramas caídas: accidental</p>
                            <ul class="details">
                                <li>Hoy, 16:00hs</li>
                                <li class="icon-camera"><span></span></li>
                                <li class="icon-comments"><span></span></li>
                                <li class="icon-tags"><span></span></li>
                            </ul>                                                       
                        </div>
                        </a> 
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-alcantarillas"></div>
                        <div class="it-r-info">
                            <h3 class="alcantarillas">alcantarillas y registros</h3>
                            <p>Alcantarilla obstruida</p>
                            <ul class="details">
                                <li>Hoy, 19:00hs</li>
                                <li class="icon-camera"><span></span></li>
                                <li class="icon-comments"><span></span></li>
                                <li class="icon-tags"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-basura"></div>
                        <div class="it-r-info">
                            <h3 class="basura">Basura, Escombros y ferias</h3>
                            <p>Levante de bolsas de barrido</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-desarrollo"></div>
                        <div class="it-r-info">
                            <h3 class="desarrollo">Desarrollo</h3>
                            <p>Problema de desarrollo 2</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-barometrica"></div>
                        <div class="it-r-info">
                            <h3 class="barometrica">Barometrica</h3>
                            <p>Solicitud de barométrica</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Reporte final</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                </div>
                <!--/end reports list-->
            </div>
        </div>
        <!--end listado de reportes-->
        
        
        
        <!--MAP-->
        <!--<div class="map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3271.959063955584!2d-56.186689099999995!3d-34.9074768!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x959f81cb87dc458b%3A0x2da7d92a1efcbe7b!2sEjido!5e0!3m2!1ses!2suy!4v1401075267114" width="100%" height="100%" frameborder="0" style="border:0"></iframe>        </div>-->
        <!--end map-->
    </div>
    <!--/main container-->

	<!--SCRIPTS-->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
	<script src="js/jquery.slimscroll.js"></script>
    <script src="js/jquery.responsiveText.js"></script>
	<script src="js/main.js"></script>
    <script src="js/actions.js"></script>
    
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5065c0290cc90532"></script>
    
</body>
</html>
