        <div class="reportes new-report sub" id="add-report">
		<div class="c-scroll-reportar"><!--nuevo-->
            <div class="scrolled-reportar">
            <div class="c-form-report">
                <div class="ir">
                    <h2>Realizar un reporte</h2>
                    <p>Donec quis turpis et elit vehicula dignissim ut quis lectus. Aenean gravida nisi vitae enim dignissim imperdiet. 
                    Duis varius justo purus, et lobortis turpis sagittis sed.</p>
                </div>
                <form role="form">
                  <!--<div class="form-group">
                    <label class="active" for="direccion">Dirección del problema</label>
                    <input class="form-control" type="text" id="direccion" placeholder="Calle">
                    <input class="form-control" type="text" placeholder="Esquina/Número">
                  </div>-->                            
                    <!--Category-->
                    <div class="form-group">
                        <label class="active" for="categoria">Categoría & subcategoría</label>
                        <select id="categoria" class="form-control">
                            <option class="arbolado">Arbolado</option>
                            <option class="alcantarillas">Alcantarillas y registros</option>
                            <option class="basura">Basura, Escombros y ferias</option>
                            <option class="desarrollo">Desarrollo</option>
                            <option class="barometrica">Barométrica</option>
                            <option class="vereda">Vereda</option>
                        </select>
                        <!--Subcategory-->
                        <select id="categoria" class="form-control">
                            <option>Alcantarilla obstruida</option>
                            <option>Alcantarilla rota</option>
                            <option>Registro nuevo</option>
                            <option>Registro roto</option>
                        </select>
                    </div>
                    <!--Description-->
                    <div class="form-group">
                        <label for="descripcion">Descripción</label>
                        <textarea class="form-control" id="descripcion" rows="4"></textarea>
                    </div>
                    <!--Images-->
                    <div class="form-group">
                        <label for="foto">Agregar una foto</label>
                        <div class="drag">
                            <span>Arrastra aquí & Clic para explorar</span>
                        </div>
                        <p class="help-block">* El archivo no puede pesar más de 2MB (JPG, PNG, GIF)</span>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-reportar btn-disabled" disabled="disabled">Publicar reporte</button>
                        <!--Cuando el usuario inicia sesion, remover la clase btn-disabled-->
                    </div>
                </form>
            </div>
            <!--INICIAR SESION-->
            <div class="login">
                <div class="bloque-sesion">
                    <div class="ir">
                        <h2>Accede a tu cuenta</h2>
                        <p>Conectate a tu cuenta para poder finalizar tu reporte y enviarlo a la Intendencia.</p>
                    </div>
                    <div class="social-buttons">
                        <button class="btn btn-block btn-social btn-facebook">Registrate con Facebook</button>
                        <button class="btn btn-block btn-social btn-twitter">Registrate con Twitter</button>
                    	<p>...ó ingresa los datos si ya tienes cuenta:</p>
                    </div>
                    <form role="form">
                        <div class="form-group">
                            <input class="form-control error" type="text" placeholder="Correo electrónico">                            	
                            <input class="form-control" type="password" placeholder="Contraseña">                            	
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-reportar">Iniciar sesión</button>
                        </div>
                        <p class="no-account">¿No utilizas redes sociales? No hay problema!
                            <br />                        
                            <a class="green registrate" href="javascript:;">Regístrate aquí con tu mail</a>
                        </p>
                        <!--<div class="form-group">
                            <a href="javascript:;" class="btn btn-block btn-green registrate">Regístrate aquí</a>
                        </div>-->
                    </form>
                </div>
                <!--end iniciar sesion-->
                <!--LOGIN-->
                <div class="bloque-registro">
                    <div class="ir">
                        <h2>Regístrate y se parte</h2>
                        <p>Crea una cuenta en PorMiBarrio y comienza a colaborar.</p>
                    </div>
                    <form role="form">
                        <div class="form-group">
                            <input class="form-control" type="text" placeholder="Nombre y apellido">                            	
                            <input class="form-control" type="text" placeholder="Cédula de Identidad">                            	
                            <input class="form-control" type="email" placeholder="Correo Electrónico" >
                            <input class="form-control" type="password" placeholder="Contraseña" >
                        </div> 
                        <div class="checkbox">
                            <label>
                              <input type="checkbox"> Al continuar con tu registro, aceptas haber leído y aceptado nuestra 
                              <a href="javascript:;"> Política de privacidad</a>.
                            </label>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-green">Regístrate</button>
                        </div>
                      </form>
                </div>
            </div>
            </div>                
            <!--end login-->
            </div>                
        </div>
        <!--end reportar e iniciar sesion-->
