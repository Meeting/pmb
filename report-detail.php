<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Por mi Barrio</title>

    <!--CSS-->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/main.css" rel="stylesheet" />
    <link href="css/sprites.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet" />
    
    <!--FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body>
<svg display="none" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="264" height="24" viewBox="0 0 264 24">
<defs>
<g id="icon-icono1">
	<path class="path1" d="M6.573 8.625c-0.865 0-1.566 0.701-1.566 1.566s0.701 1.567 1.566 1.567c0.865 0 1.566-0.701 1.566-1.567s-0.701-1.566-1.566-1.566zM6.573 11.131c-0.519 0-0.94-0.421-0.94-0.94s0.42-0.939 0.94-0.939c0.519 0 0.94 0.421 0.94 0.939s-0.422 0.94-0.94 0.94z"></path>
	<path class="path2" d="M20.356 2.047h-16.289c-1.039 0-1.88 0.842-1.88 1.88v16.288c0 1.038 0.841 1.878 1.88 1.878h16.289c1.039 0 1.879-0.84 1.879-1.878v-16.288c0-1.038-0.84-1.88-1.879-1.88zM21.61 19.901c0 0.867-0.702 1.567-1.566 1.567h-15.664c-0.865 0-1.566-0.7-1.566-1.567v-15.662c0-0.865 0.701-1.567 1.566-1.567h15.663c0.865 0 1.566 0.701 1.566 1.567l0.001 15.662z"></path>
	<path class="path3" d="M19.103 6.432c0 0.172-0.141 0.313-0.314 0.313h-9.397c-0.172 0-0.313-0.141-0.313-0.313v0c0-0.173 0.141-0.313 0.313-0.313h9.397c0.173 0 0.314 0.14 0.314 0.313v0z"></path>
	<path class="path4" d="M19.103 10.191c0 0.173-0.141 0.313-0.314 0.313h-9.397c-0.172 0-0.313-0.141-0.313-0.313v0c0-0.172 0.141-0.313 0.313-0.313h9.397c0.173 0 0.314 0.141 0.314 0.313v0z"></path>
	<path class="path5" d="M19.103 13.95c0 0.172-0.141 0.311-0.314 0.311h-9.397c-0.172 0-0.313-0.139-0.313-0.311v0c0-0.173 0.141-0.314 0.313-0.314h9.397c0.173 0 0.314 0.141 0.314 0.314v0z"></path>
	<path class="path6" d="M19.103 17.709c0 0.172-0.141 0.313-0.314 0.313h-9.397c-0.172 0-0.313-0.141-0.313-0.313v0c0-0.172 0.141-0.312 0.313-0.312h9.397c0.173 0 0.314 0.14 0.314 0.312v0z"></path>
	<path class="path7" d="M7.199 13.95c0 0.346-0.281 0.627-0.627 0.627s-0.627-0.281-0.627-0.627c0-0.346 0.281-0.627 0.627-0.627s0.627 0.281 0.627 0.627z"></path>
	<path class="path8" d="M7.199 6.432c0 0.346-0.281 0.627-0.627 0.627s-0.627-0.281-0.627-0.627c0-0.346 0.281-0.627 0.627-0.627s0.627 0.281 0.627 0.627z"></path>
	<path class="path9" d="M7.198 17.709c0 0.346-0.28 0.626-0.626 0.626s-0.626-0.28-0.626-0.626c0-0.346 0.28-0.626 0.626-0.626s0.626 0.28 0.626 0.626z"></path>
</g>
<g id="icon-icono2">
	<path class="path1" d="M5.613 14.995l-0.483 0.484-3.385-3.385 3.385-3.383 0.483 0.483-2.898 2.9z"></path>
	<path class="path2" d="M2.446 11.752h12.411v0.685h-12.411v-0.685z"></path>
	<path class="path3" d="M21.794 4.73v14.681h-14.684v-5.452h0.684v4.77h13.314v-13.316h-13.314v4.627h-0.684v-5.31z"></path>
</g>
<g id="icon-icono3">
	<path class="path1" d="M20.271 4.303h-16.172c-1.029 0-1.866 0.835-1.866 1.866v11.195c0 1.030 0.837 1.867 1.866 1.867h2.116l1.347 2.334c0.003 0.004 0.006 0.009 0.009 0.013 0.007 0.011 0.014 0.019 0.021 0.030 0.006 0.008 0.013 0.015 0.018 0.019 0.007 0.009 0.015 0.016 0.023 0.022 0.009 0.008 0.017 0.013 0.025 0.018 0.005 0.004 0.012 0.009 0.016 0.012 0.003 0.002 0.007 0.003 0.010 0.004 0.009 0.005 0.019 0.010 0.028 0.014 0.009 0.005 0.020 0.009 0.027 0.011 0.010 0.003 0.020 0.005 0.029 0.007s0.020 0.003 0.030 0.004c0.011 0.002 0.021 0.002 0.029 0.002 0.011 0 0.021 0 0.031-0.002s0.021-0.003 0.030-0.004c0.010-0.002 0.019-0.004 0.027-0.007 0.010-0.003 0.019-0.006 0.028-0.011 0.010-0.003 0.019-0.009 0.029-0.015 0.004-0.001 0.007-0.003 0.010-0.004 0.005-0.003 0.011-0.008 0.015-0.012 0.009-0.005 0.018-0.010 0.027-0.018 0.009-0.006 0.015-0.013 0.022-0.020 0.005-0.007 0.012-0.014 0.019-0.020 0.007-0.010 0.014-0.018 0.021-0.029 0.003-0.004 0.007-0.008 0.009-0.014l1.348-2.335h10.823c1.029 0 1.866-0.836 1.866-1.866v-11.196c0.003-1.031-0.832-1.866-1.864-1.866zM21.516 17.053c0 0.859-0.696 1.555-1.554 1.555h-10.874l-0.358 0.623-0.899 1.556-1.256-2.179h-2.163c-0.861 0-1.556-0.697-1.556-1.555v-10.573c0-0.859 0.696-1.555 1.556-1.555h15.549c0.859 0 1.554 0.696 1.554 1.555v10.573z"></path>
	<path class="path2" d="M12.186 10.212c-0.859 0-1.555 0.696-1.555 1.554s0.697 1.555 1.555 1.555c0.861 0 1.556-0.697 1.556-1.555s-0.695-1.554-1.556-1.554zM12.186 12.698c-0.515 0-0.933-0.417-0.933-0.932s0.417-0.933 0.933-0.933c0.516 0 0.934 0.417 0.934 0.933s-0.421 0.932-0.934 0.932z"></path>
	<path class="path3" d="M17.475 10.212c-0.86 0-1.557 0.696-1.557 1.554s0.697 1.555 1.557 1.555c0.859 0 1.554-0.697 1.554-1.555s-0.695-1.554-1.554-1.554zM17.475 12.698c-0.517 0-0.934-0.417-0.934-0.932s0.417-0.933 0.934-0.933c0.514 0 0.933 0.417 0.933 0.933s-0.421 0.932-0.933 0.932z"></path>
	<path class="path4" d="M6.899 10.212c-0.859 0-1.554 0.696-1.554 1.554s0.696 1.555 1.554 1.555c0.86 0 1.556-0.697 1.556-1.555s-0.696-1.554-1.556-1.554zM6.899 12.698c-0.514 0-0.933-0.417-0.933-0.932s0.419-0.933 0.933-0.933c0.515 0 0.933 0.417 0.933 0.933s-0.418 0.932-0.933 0.932z"></path>
</g>
<g id="icon-icono4">
	<path class="path1" d="M11.744 21.896c-5.404 0-9.801-4.397-9.801-9.801s4.397-9.801 9.801-9.801c5.405 0 9.802 4.396 9.802 9.801s-4.397 9.801-9.802 9.801zM11.744 2.966c-5.033 0-9.128 4.095-9.128 9.129s4.095 9.129 9.128 9.129c5.034 0 9.127-4.095 9.127-9.129s-4.093-9.129-9.127-9.129z"></path>
	<path class="path2" d="M11.744 21.896c-2.383 0-4.682-0.868-6.472-2.445l-0.248-0.219 0.211-0.251c0.631-0.745 1.741-1.366 3.13-1.749 0.496-0.196 0.736-0.328 0.915-0.507 0.191-0.193 0.297-0.373 0.335-0.571 0.025-0.134 0.018-0.85-0.002-1.485-0.345-0.39-0.645-0.973-0.933-1.809-0.297-0.158-0.56-0.535-0.639-0.952-0.084-0.448-0.037-0.826 0.123-1.050-0.366-2.153-0.129-3.752 0.705-4.754 0.652-0.783 1.649-1.179 2.965-1.179l0.117-0.001c1.098 0 1.349 0.229 1.587 0.553 0.048 0.063 0.083 0.113 0.139 0.159 0.606 0 1.108 0.244 1.454 0.702 0.658 0.871 0.736 2.537 0.221 4.487 0.182 0.223 0.232 0.625 0.127 1.106-0.1 0.446-0.415 0.842-0.732 0.965-0.232 0.768-0.473 1.349-0.847 1.787v1.405c0.041 0.309 0.159 0.423 0.373 0.632l0.142 0.142c0.135 0.106 0.251 0.163 0.783 0.397 1.331 0.369 2.431 0.986 3.054 1.725l0.21 0.251-0.245 0.218c-1.79 1.575-4.089 2.444-6.473 2.444zM5.982 19.172c1.625 1.326 3.659 2.052 5.762 2.052 2.104 0 4.137-0.726 5.764-2.052-0.567-0.531-1.451-0.981-2.537-1.282-0.59-0.26-0.772-0.341-1.010-0.535l-0.157-0.155c-0.248-0.24-0.502-0.488-0.576-1.073l-0.002-1.709 0.093-0.098c0.372-0.387 0.608-1.016 0.847-1.839l0.137-0.468 0.271 0.206c0.082-0.075 0.197-0.231 0.244-0.433 0.054-0.249 0.041-0.427 0.020-0.506l-0.256-0.081 0.054-0.321c0.52-1.822 0.504-3.405-0.045-4.133-0.223-0.292-0.522-0.435-0.921-0.435l-0.182 0.006-0.089-0.052c-0.219-0.129-0.328-0.279-0.409-0.388-0.114-0.155-0.202-0.278-1.043-0.278l-0.109 0.001c-1.113 0-1.937 0.315-2.455 0.938-0.724 0.87-0.903 2.344-0.529 4.383l0.061 0.336-0.224 0.037c-0.021 0.083-0.038 0.255 0.008 0.493 0.052 0.27 0.232 0.46 0.293 0.488l0.252-0.010 0.021 0.233c0.297 0.896 0.587 1.47 0.916 1.803l0.092 0.092 0.006 0.132c0.012 0.339 0.046 1.473-0.006 1.757-0.063 0.333-0.232 0.633-0.519 0.923-0.285 0.283-0.635 0.452-1.179 0.668-1.125 0.314-2.021 0.766-2.593 1.303z"></path>
</g>
<g id="icon-icono5">
	<path class="path1" d="M20.982 19.327h-17.643c-0.482 0-0.872-0.388-0.872-0.87v-12.226c0-0.481 0.39-0.871 0.872-0.871h17.643c0.481 0 0.87 0.39 0.87 0.871v12.227c-0.001 0.481-0.39 0.869-0.87 0.869zM4.209 17.587h15.9v-10.485h-15.9v10.485z"></path>
	<path class="path2" d="M12.159 15.609c-1.929 0-3.5-1.571-3.5-3.501s1.571-3.5 3.5-3.5c1.931 0 3.501 1.57 3.501 3.5s-1.57 3.501-3.501 3.501zM12.159 10.35c-0.969 0-1.758 0.789-1.758 1.758 0 0.968 0.789 1.757 1.758 1.757 0.97 0 1.757-0.789 1.757-1.757 0-0.969-0.789-1.758-1.757-1.758z"></path>
	<path class="path3" d="M8.239 6.666h-3.485c-0.24 0-0.434-0.195-0.434-0.436v-2.403c0-0.242 0.194-0.435 0.434-0.435h3.485c0.241 0 0.436 0.195 0.436 0.435v2.403c0 0.241-0.195 0.436-0.436 0.436zM5.19 5.795h2.614v-1.534h-2.614v1.534z"></path>
</g>
<g id="icon-icono6">
	<path class="path1" d="M6.661 20.323c-0.5 0-0.946-0.295-1.143-0.749-0.015-0.037-0.027-0.072-0.039-0.108-0.128-0.457-0.012-0.938 0.303-1.257 0.004-0.002 0.614-0.645 0.899-1.736-2.103-1.449-3.34-3.621-3.34-5.911 0-4.231 4.144-7.671 9.237-7.671s9.236 3.441 9.236 7.671-4.143 7.671-9.236 7.671c-0.354 0-0.721-0.019-1.099-0.054-1.124 1.038-2.795 2.143-4.818 2.143zM12.577 4.663c-4.119 0-7.467 2.646-7.467 5.9 0 1.871 1.096 3.595 3.007 4.727 0.307 0.181 0.473 0.528 0.427 0.88-0.122 0.912-0.423 1.655-0.726 2.201 1.114-0.334 2.059-1.089 2.72-1.748 0.195-0.196 0.47-0.286 0.743-0.251 0.45 0.058 0.888 0.089 1.296 0.089 4.116 0 7.465-2.649 7.465-5.899 0-3.254-3.349-5.9-7.465-5.9z"></path>
	<path class="path2" d="M16.488 9.789h-7.536c-0.243 0-0.443-0.198-0.443-0.444s0.199-0.442 0.443-0.442h7.536c0.244 0 0.442 0.197 0.442 0.442s-0.198 0.444-0.442 0.444z"></path>
	<path class="path3" d="M16.488 12.384h-7.536c-0.243 0-0.443-0.197-0.443-0.443s0.199-0.444 0.443-0.444h7.536c0.244 0 0.442 0.197 0.442 0.444 0 0.245-0.198 0.443-0.442 0.443z"></path>
</g>
<g id="icon-icono7">
	<path class="path1" d="M13.331 21c-0.192 0-0.381-0.071-0.529-0.218l-9.298-9.296c-0.161-0.162-0.239-0.391-0.213-0.618l0.734-6.243c0.041-0.348 0.316-0.621 0.665-0.657l6.166-0.657c0.224-0.024 0.447 0.055 0.609 0.214l9.297 9.297c0.142 0.14 0.219 0.331 0.219 0.53 0 0.196-0.077 0.387-0.219 0.53l-6.901 6.901c-0.147 0.147-0.339 0.218-0.531 0.218zM4.821 10.683l8.511 8.51 5.842-5.84-8.514-8.515-5.216 0.557-0.623 5.288z"></path>
	<path class="path2" d="M7.853 9.059c-0.688 0-1.249-0.561-1.249-1.249s0.561-1.249 1.249-1.249c0.689 0 1.25 0.56 1.25 1.249 0 0.687-0.561 1.249-1.25 1.249zM7.853 7.311c-0.274 0-0.5 0.224-0.5 0.5s0.225 0.5 0.5 0.5c0.275 0 0.499-0.225 0.499-0.5s-0.224-0.5-0.499-0.5z"></path>
	<path class="path3" d="M13.462 15.539c-0.097 0-0.192-0.035-0.267-0.109l-3.66-3.661c-0.147-0.146-0.147-0.381 0-0.53 0.147-0.146 0.381-0.146 0.529 0l3.661 3.662c0.146 0.146 0.146 0.382 0 0.529-0.070 0.074-0.168 0.109-0.262 0.109z"></path>
	<path class="path4" d="M14.961 14.042c-0.097 0-0.192-0.036-0.267-0.11l-3.661-3.661c-0.147-0.147-0.147-0.384 0-0.53s0.382-0.147 0.53 0l3.661 3.66c0.146 0.147 0.146 0.382 0 0.531-0.071 0.074-0.166 0.11-0.262 0.11z"></path>
</g>
<g id="icon-icono8">
	<path class="path1" d="M17.857 3.732c-1.79-1.785-4.169-2.767-6.695-2.764-2.529 0.002-4.91 0.991-6.704 2.785-1.785 1.789-2.768 4.17-2.766 6.696 0.003 2.529 0.989 4.908 2.781 6.698l6.154 6.147c0.143 0.135 0.336 0.221 0.549 0.221 0.217 0 0.414-0.088 0.557-0.227l0.018-0.018 6.125-6.135c1.786-1.791 2.77-4.169 2.768-6.699-0.003-2.526-0.992-4.907-2.787-6.703zM16.911 16.17l-5.163 5.17c0 0 0 0 0-0.001-0.147 0.149-0.35 0.241-0.573 0.241-0.225 0.002-0.429-0.092-0.573-0.24v0l-5.164-5.159c-1.533-1.533-2.379-3.567-2.381-5.734-0.001-2.163 0.839-4.2 2.367-5.732 1.536-1.537 3.573-2.382 5.739-2.385 2.163-0.001 4.197 0.839 5.732 2.366 1.537 1.538 2.384 3.577 2.385 5.741 0.003 2.163-0.838 4.199-2.367 5.733z"></path>
	<path class="path2" d="M15.206 10.185c0 2.211-1.793 4.004-4.004 4.004s-4.004-1.793-4.004-4.004c0-2.211 1.793-4.004 4.004-4.004s4.004 1.793 4.004 4.004z"></path>
</g>
</defs></svg>
	<!--MAIN CONTAINER-->
    <div class="container">
    	<!--FIRST LEVEL NAVIGATION-->
        <nav class="first-navigation">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img class="img-responsive" src="images/bg/PMB-small.png" alt=""></a>
            <ul class="r-small">
                <li class="reportar reportar-small"><a href="reportar.php"><svg class="icon icon-icono8" viewBox="0 0 24 24"><use xlink:href="#icon-icono8"></use></svg><span>Reportar</span></a></li>
            </ul>
            <div class="navbar-collapse collapse">
                <ul>
                    <li class="home"><a href="index.php"><span></span></a></li>
                    <li class="reportar"><a href="reportar.php"><svg class="icon icon-icono8" viewBox="0 0 24 24"><use xlink:href="#icon-icono8"></use></svg> <span>Reportar</span></a></li>
                    <li class="reportes"><a href="lista-reportes.php"><svg class="icon icon-icono1" viewBox="0 0 24 24"><use xlink:href="#icon-icono1"></use></svg> <span>Reportes</span></a></li>
                    <li class="help"><a href="info-help.php"><svg class="icon icon-icono3" viewBox="0 0 24 24"><use xlink:href="#icon-icono3"></use></svg> <span>Ayuda</span></a></li>
                    <li class="profile"><a href="profile.php"><img class="img-circle" width="35" height="35" src="images/user-profile-ref.png"/><span>Perfil</span></a></li>
                </ul>
                <ul class="help-info">
                    <!-- <li class="doc-info"><a href="javascript:;">Doc info</a></li> -->
                    <li class="data"><a href="javascript:;">DATA</a></li>
                    <li class="my-society"><a href="javascript:;">My Society</a></li>
                </ul>
            </div>
        </nav>
        
        
        <!--DETALLE DE REPORTE-->
        <div class="c-report">
            <div class="container-report">
                <!--REPORT-->
                <div class="report">
                    <div class="info-report scrolled-report">
                    	<a href="javascript:;" class="btn btn-close-report">Cerrar</a>
                        <div class="image-report">
                            <img class="img-top" src="images/ref-hotel.jpg" alt=""/>
                        </div>
                        <!--Intro Report-->
                        <div class="intro-report">
                            <h3>Alcantarilla obstruida</h3>
                            <address>Bv. Gral. Artigas 1234 esq Eduardo Victor Haedo</address>
                            <p>
                            Donec quis turpis et elit vehicula dignissim ut quis lectus. Aenean gravida nisi vitae enim dignissim 
                            imperdiet. Ut eu viverra odio. Integer commodo massa mi, sit amet 
                            rutrum nunc vestibulum at. Integer viverra rhoncus mauris id laoreet. 
                            </p>
                            <div class="sign">
                       			<div class="picture">
                                	<img class="img-circle" src="images/user-ref.jpg" width="35" height="35" alt="Roberto Ricci">
                                </div>
                                <div class="author">
                            		<span>Publicado por <a href="javascript:;">Roberto Ricci</a>, ayer a las 19:00hs</span>
                                </div>
                            </div>
                        </div>
                        <!--Status Report-->
                        <div class="status-report">
                            <h4>Estado del reporte</h4>
                            <ul>
                                <li>
                                    Cuadrilla asignada para el 16/05/2014
                                    <span class="time-report">hace 1 hr</span>
                                </li>
                                <li>
                                    Problema ingresado al sistema
                                    <span class="time-report">hace 4 días</span>
                                </li>
                            </ul>
                        </div>
                        <!-- -->
                        <div class="actions">
                            <ul class="actions">
                                <li><button type="button" class="follow-report">Seguir de cerca este reporte</button></li>
                                <li><button type="button" class="reportar-abuso">Reportar abuso</button></li>
                            </ul>
                            <div class="share">
                                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                                <div class="addthis_sharing_toolbox"></div>                            
                            </div>
                            <div class="clearfix"></div>
                            <div class="follow-report-content user-info">
                            	<form role="form">
                                	<p>Reciba un correo cuando se dejen actualizaciones sobre este problema.</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Nombre">
                                        <input type="email" class="form-control" placeholder="Email">
                                        <button type="submit" class="btn btn-sky btn-small btn-comentar">Suscribirse</button>
                                    </div>                                        
                                </form>
                            </div>
                            <div class="reportar-abuso-content user-info">
                            	<form role="form">
                                	<p>Estás reportando por abusiva la siguiente notificación, que contiene información personal, o similar: <b>Alcantarilla obstruida</b></p>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Nombre">
                                        <input type="email" class="form-control" placeholder="Email">
                                        <input type="text" class="form-control" placeholder="Títutlo">
                                        <textarea class="form-control" rows="4" style="height:auto;" placeholder="Mensaje"></textarea>
                                        <button type="submit" class="btn btn-sky btn-small btn-comentar">Publicar</button>
                                    </div>                                        
                                </form>
                            </div>
                        </div>
                        <!-- / -->
                        <!--Comments Report-->
                        <div class="comment-reports">
                            <h3>Comentarios</h3>
                            <div class="comment">
                                <div class="user-avatar"><img src="images/user-2.png" alt=""/></div>
                                <div class="user-info">
                                    <h4>Zlatan Ibrahimovic</h4>
                                    <span class="time-report">hace 2 días</span>
                                </div>
                                <div class="user-comment">
                                    <p>
                                        "You remember when your dad taught you to swim?" The boy nodded. "Well, in the University of 
                                        California” - that is the name we had for the houses - we taught young men and women 
                                        how to think, just as I have taught you now, by sand and pebbles and shells, to 
                                        know how many 
                                    </p>
                                </div>                            
                            </div>
                            <div class="comment imm-comment">
                                <div class="user-avatar"><img src="images/user-imm.png" alt=""/></div>
                                <div class="user-info">
                                    <h4>Intendencia de Montevideo</h4>
                                    <span class="time-report">hace 4 días</span>
                                </div>
                                <div class="user-comment">
                                    <p>
                                        La cuadrilla #12345 fue asignada para resolver este problema para el 16/05. Cualquier consulta...
                                    </p>
                                </div>                            
                            </div>
                            <div class="comment">
                                <div class="user-avatar"><img src="images/user-3.png" alt=""/></div>
                                <div class="user-info">
                                    <h4>Olivia Wilde</h4>
                                    <span class="time-report">hace 2 meses</span>
                                </div>
                                <div class="user-comment">
                                    <p>
                                        Ok, I saw this yesterday.
                                    </p>
                                </div> 
                                <div class="user-picture">
                                    <img class="img-responsive" src="images/ref-hotel.jpg" alt=""/>
                                </div>                           
                            </div>
                            <div class="comment imm-comment">
                                <div class="user-avatar"><img src="images/user-imm.png" alt=""/></div>
                                <div class="user-info">
                                    <h4>Intendencia de Montevideo</h4>
                                    <span class="time-report">hace 4 días</span>
                                </div>
                                <div class="user-comment">
                                    <p>
                                        La cuadrilla #12345 fue asignada para resolver este problema para el 16/05. Cualquier consulta...
                                    </p>
                                </div>                            
                            </div>
                            <!--INICIAR SESION-->
                            <div class="login">
                                <div class="bloque-sesion new-report">
                                    <div class="ir">
                                        <h2>Accede a tu cuenta</h2>
                                        <p>Conectate a tu cuenta para poder finalizar tu reporte y enviarlo a la Intendencia.</p>
                                    </div>
                                    <div class="social-buttons">
                                        <button class="btn btn-block btn-social btn-facebook">Registrate con Facebook</button>
                                        <button class="btn btn-block btn-social btn-twitter">Registrate con Twitter</button>
                                        <p>...ó ingresa los datos si ya tienes cuenta:</p>
                                    </div>
                                    <form role="form">
                                        <div class="form-group">
                                            <input class="form-control error" type="text" placeholder="Correo electrónico">                            	
                                            <input class="form-control" type="password" placeholder="Contraseña">                            	
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-block btn-reportar">Iniciar sesión</button>
                                        </div>
                                        <p class="no-account">¿No utilizas redes sociales? No hay problema!
                                            <br />                        
                                            <a class="green registrate" href="javascript:;">Regístrate aquí con tu mail</a>
                                        </p>
                                        <!--<div class="form-group">
                                            <a href="javascript:;" class="btn btn-block btn-green registrate">Regístrate aquí</a>
                                        </div>-->
                                    </form>
                                </div>
                                <!--end iniciar sesion-->
                                <!--LOGIN-->
                                <div class="bloque-registro new-report">
                                    <div class="ir">
                                        <h2>Regístrate y se parte</h2>
                                        <p>Crea una cuenta en PorMiBarrio y comienza a colaborar.</p>
                                    </div>
                                    <form role="form">
                                        <div class="form-group mas-padding">
                                            <input class="form-control" type="text" placeholder="Nombre y apellido">                            	
                                            <input class="form-control" type="text" placeholder="Cédula de Identidad">                            	
                                            <input class="form-control" type="email" placeholder="Correo Electrónico" >
                                            <input class="form-control" type="password" placeholder="Contraseña" >
                                        </div> 
                                        <div class="checkbox">
                                            <label>
                                              <input type="checkbox"> Al continuar con tu registro, aceptas haber leído y aceptado nuestra 
                                              <a href="javascript:;"> Política de privacidad</a>.
                                            </label>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-block btn-green">Regístrate</button>
                                        </div>
                                      </form>
                                </div>
                            </div>
                            </div>                
                            <!--end login-->
							<!--User comments here-->
                            <div class="comment comment-user">
                                <div class="user-avatar">
                                	<img class="img-circle" src="images/no-profile-picture.jpg" alt=""/>
                                </div>
                                <div class="user-info comments-user-info">
                                    <!--nuevo-->
                                    <form role="form">
                                        <div class="form-group">
                                        	<input class="form-control" placeholder="Nombre" />
                                        	<input class="form-control" placeholder="Email" />
                                            <button type="button" class="upload-img">Añadir imagen</button>
                                            <input type="file" id="InputFile">
                                            <div class="boxs">
                                                <div class="checkbox">
                                                    <label>
                                                      <input type="checkbox">Marcar este problema como resuelto.                               
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label>
                                                      <input type="checkbox">Mostrar nombre público.                               
                                                    </label>
                                                </div>
                                            </div>
                                            <textarea rows="" placeholder="Escriba aquí su comentario"></textarea>
                                            <button type="submit" class="btn btn-sky btn-small btn-comentar btn-disabled" disabled="disabled">Comentar</button>
                                            <!--Cuando el usuario inicia sesion, remover la clase btn-disabled-->
                                            <!--<a href="javascript:;" class="btn btn-photo"></a>-->
                                            <div class="clearfix"></div>
                                        </div>                                        
                                    </form>
                                </div>                            
                            </div>
                        </div>
                    </div>
                </div>
                <!--end report-->
            </div>
            <!--end container report-->   
        </div>
        <!--end detalle de reporte report-->         
        
        
        <!--MAP-->
        <!--<div class="map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3271.959063955584!2d-56.186689099999995!3d-34.9074768!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x959f81cb87dc458b%3A0x2da7d92a1efcbe7b!2sEjido!5e0!3m2!1ses!2suy!4v1401075267114" width="100%" height="100%" frameborder="0" style="border:0"></iframe>        </div>-->
        <!--end map-->
    </div>
    <!--/main container-->

	<!--SCRIPTS-->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
	<script src="js/jquery.slimscroll.js"></script>
    <script src="js/jquery.responsiveText.js"></script>
    <script src="js/actions.js"></script>
    <script src="js/jquery.localScroll.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    
	<script src="js/main.js"></script>
    <script src="js/actions.js"></script>
    <script src="js/bootstrap-select.js"></script>

	<script type="text/javascript">
      window.onload = function () {
        $('.selectpicker').selectpicker();
      };
    </script>

    
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5065c0290cc90532"></script>
    
</body>
</html>
