<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Por mi Barrio</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">

    <!--CSS-->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/main.css" rel="stylesheet" />
    <link href="css/sprites.css" rel="stylesheet">
    <link href="css/bootstrap-combined.min.css" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-select.css">
    
    <!--FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body>
<?php include ('inc/sprites.php'); ?>
	<!--MAIN CONTAINER-->
    <div class="container">
    	<!--FIRST LEVEL NAVIGATION-->
        <nav class="first-navigation">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img class="img-responsive" src="images/bg/PMB-small.png" alt=""></a>
            <ul class="r-small">
                <li class="reportar reportar-small"><a href="reportar.php"><svg class="icon icon-icono8" viewBox="0 0 24 24"><use xlink:href="#icon-icono8"></use></svg><span>Reportar</span></a></li>
            </ul>
            <div class="navbar-collapse collapse">
                <ul>
                    <li class="home"><a href="index.php"><span></span></a></li>
                    <li class="reportar"><a href="reportar.php"><svg class="icon icon-icono8" viewBox="0 0 24 24"><use xlink:href="#icon-icono8"></use></svg> <span>Reportar</span></a></li>
                    <li class="reportes"><a href="lista-reportes.php"><svg class="icon icon-icono1" viewBox="0 0 24 24"><use xlink:href="#icon-icono1"></use></svg> <span>Reportes</span></a></li>
                    <li class="help"><a href="info-help.php"><svg class="icon icon-icono3" viewBox="0 0 24 24"><use xlink:href="#icon-icono3"></use></svg> <span>Ayuda</span></a></li>
                    <li class="profile"><a href="profile.php"><img class="img-circle" width="35" height="35" src="images/user-profile-ref.png"/><span>Perfil</span></a></li>
                </ul>
                <ul class="help-info">
                    <!-- <li class="doc-info"><a href="javascript:;">Doc info</a></li> -->
                    <li class="data"><a href="javascript:;">DATA</a></li>
                    <li class="my-society"><a href="javascript:;">My Society</a></li>
                </ul>
            </div>
        </nav>
        
        <!--PROFILE-->
        <div class="reportes sub report-list user-profile" id="user-profile" >
            <!--Intro reports-->
            <div class="ir" id="top-profile">
                <div class="c-t profile">
                	<a href="editar-perfil.php" class="btn btn-sky btn-small btn-comentar btn-editar">Editar</a>
                    <div class="img-profile">
                    	<img class="img-circle" src="images/user-profile-ref.png" alt="Roberto Carlo" />                    	
                    </div>                    
                    <h2>Roberto Carlo</h2>
                    <ul>
                    	<li>Periodista</li>
                        <li class="dot"><a href="javascript:;">La Blanquedada</a></li>
                    </ul>
                    <p>Amo el barrio, los vecinos y la tranquilidad. Siempre que puedo trato de aportar mi granito de arena. 
                    Hagamos de Montevideo un mejor lugar!</p>
                    <!--<div class="rep-filtro">
                    	<button type="button" id="tus-reportes" class="btn btn-filtro btn-filtro-active"><svg class="icon icon-icono1" viewBox="0 0 24 24"><use xlink:href="#icon-icono1"></use></svg><br />tus reportes<br /><span>Que tú has publicado</span></button>
                        <button type="button" id="siguiendo" class="btn btn-filtro"><svg class="icon icon-icono9" viewBox="0 0 24 24"><use xlink:href="#icon-icono9"></use></svg><br />Siguiendo<br /><span>Reportes de otros</span></button>
                    </div>-->
                </div>
            </div>
            <!--/end intro reports-->
            <!--REPORTS LIST-->
            <div class="c-scroll-user">
                <div class="rl scrolled scrolled-user">                    	
                    <p class="no-reportes">No tiene ningún reporte aún.</p>
                </div>
                <!--/end reports list-->
            </div>
        </div>
        <!--end listado de reportes-->

        
        
        <!--MAP-->
        <!--<div class="map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3271.959063955584!2d-56.186689099999995!3d-34.9074768!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x959f81cb87dc458b%3A0x2da7d92a1efcbe7b!2sEjido!5e0!3m2!1ses!2suy!4v1401075267114" width="100%" height="100%" frameborder="0" style="border:0"></iframe>        </div>-->
        <!--end map-->
    </div>
    <!--/main container-->

	<!--SCRIPTS-->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
	<script src="js/jquery.slimscroll.js"></script>
    <script src="js/jquery.responsiveText.js"></script>
    <script src="js/actions.js"></script>
    <script src="js/jquery.localScroll.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    
	<script src="js/main.js"></script>
    <script src="js/actions.js"></script>
    <script src="js/bootstrap-select.js"></script>

	<script type="text/javascript">
      window.onload = function () {
        $('.selectpicker').selectpicker();
      };
    </script>

    
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5065c0290cc90532"></script>
    
</body>
</html>
