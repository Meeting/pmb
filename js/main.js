//HOVER PIN

$( "div.it-r" )
  .mouseover(function() {
    $('div.pin-content div').fadeOut(300, function() {
        $('div.pin-content div').html( '<img src="images/pins/pin-arbolado-active.png"/>' );
        $('div.pin-content div').fadeIn(300);
    });
  })
  .mouseout(function() {
	$('div.pin-content div').fadeOut(300, function() {
        $('div.pin-content div').html( '<img src="images/pins/pin-arbolado.png"/>' );
        $('div.pin-content div').fadeIn(300);
    });
  });

/*
$( "div.it-r" ).hover(function() {
    $('div.pin-content div').fadeOut(500, function() {
        $('div.pin-content div').html( '<img src="images/pins/pin-arbolado-active.png"/>' );
        $('div.pin-content div').fadeIn(500);
		alert('a');
    });
});
*/

//CARGAR IMAGEN
	$('.InputButton').bind("click" , function () {
        $('#InputFile').click();
    });
	
	$('.upload-img').bind("click" , function () {
        $('#InputFile').click();
    });
	
//REPORTAR A REPORTES Y VICEVERSA
	/*
	$("#reportar").click(function() {
		$("#report-list").hide("normal", function() {
			$("#add-report").show("normal");
		});
	});
	
	$("#reportes").click(function() {
		$("#add-report").hide("normal", function() {
			$("#report-list").show("normal");
		});
	});
	*/
	$("#reportar").click(function() {
		$("#report-list").removeClass( "mostrar" ).addClass( "go-left" );
		$("#add-report").removeClass( "go-left" ).addClass( "mostrar" );
	});
	
	$("#reportes").click(function() {
		$("#add-report").removeClass( "mostrar" ).addClass( "go-left" );
		$("#report-list").removeClass( "go-left" ).addClass( "mostrar" );
	});
	
	
//WIDTH SEARCH
	var anchoVentana = $( window ).width();
	var anchoUser = $("#info-user").width();
	var anchoCalles = anchoVentana - anchoUser - 80;
	$("#s-calles").width(anchoCalles);

//INDICAR LOS REPORTES ABIERTOS EN EL LISTADO
	$('div.it-r').click(function(){
		$('div.rl div.active').removeClass('active');	
		$(this).addClass('active');	
	})
	
//QUITAR BORDE AL ULTIMO BLOQUE DE COMENTARIO
	$('.leave-comment').prev().css('border', 'none')	
	$('.leave-comment').prev('.imm-comment').css('borderBottom', '#ebebeb solid 1px')	
	
//SCROLL EN EL LISTADO DE REPORTES		
	var types = ['DOMMouseScroll', 'mousewheel', 'MozMousePixelScroll', 'wheel'];
	
	var tr = $( "#top-reports").height();	
	$('.c-scroll').css({'height':(($(window).height())-tr)});	
	
	$('div.scrolled').slimScroll({
		position: 'right',
		height: 'auto',
		railVisible: true,
		alwaysVisible: true,
		railOpacity:1,
		distance:10,
		railColor: '',
		color: '#ebebeb',
		size:'8px',
		borderRadius:4,
		opacity: 1,
	});
	
	
//SCROLL EN EL PERFIL DEL USUARIO	
	var tp = $( "#top-profile").height();	
	$('.c-scroll-user').css({'height':(($(window).height())-tp)});	
	
	$('div.scrolled-user').slimScroll({
		position: 'right',
		height: 'auto',
		railVisible: true,
		alwaysVisible: true,
		railOpacity:1,
		distance:10,
		railColor: '',
		color: '#ebebeb',
		size:'8px',
		borderRadius:4,
		opacity: 1,
	});
	

//SCROLL AL INGRESAR UN REPORTE
	$('div.scrolled-reportar').slimScroll({
		position: 'right',
		height: 'auto',
		railVisible: true,
		alwaysVisible: true,
		railOpacity:1,
		distance:10,
		railColor: '',
		color: '#65788a',
		size:'8px',
		borderRadius:4,
		opacity: 1,
	});


//SCROLL EN EL DETALLE DE REPORTE	
	$('div.scrolled-report').slimScroll({
		position: 'right',
		height: 'auto',
		railVisible: true,
		alwaysVisible: true,
		railOpacity:1,
		distance:10,
		railColor: '',
		color: '#d4d4d4',
		size:'6px',
		borderRadius:4,
		opacity: 1,
	});
	
		
//MOSTRAR EL FORM DE REGISTRO
	$('.registrate').click(function(){
		$('div.bloque-registro').slideDown();
		$('div.bloque-sesion').slideUp();	
	})


//ACTIVAR SEGUIR REPORTE Y REPORTAR ABUSO
	$( ".follow-report" ).click(function() {
	  $( this ).toggleClass( "follow-report-active" );
	  $( '.reportar-abuso' ).removeClass( "reportar-abuso-active" );
	  $( '.follow-report-content' ).slideToggle();
	  $( '.reportar-abuso-content' ).slideUp();
	});
	
	$( ".reportar-abuso" ).click(function() {
	  $( this ).toggleClass( "reportar-abuso-active" );
	  $( '.follow-report' ).removeClass( "follow-report-active" );
	  $( '.reportar-abuso-content' ).slideToggle();
	  $( '.follow-report-content' ).slideUp();
	});
	
//FILTRO REPORTES

	$( "#tus-reportes" ).click(function() {
	  $( this ).addClass( "btn-filtro-active" );
	  $( "#siguiendo" ).removeClass( "btn-filtro-active" );
	});
	
	$( "#siguiendo" ).click(function() {
	  $( this ).addClass( "btn-filtro-active" );
	   $( "#tus-reportes" ).removeClass( "btn-filtro-active" );
	});


//RESPONSIVE TEXT
	$('.responsive').responsiveText();
		

//CONTRAER Y EXPANDIR BARRA AZUL
	$(".first-navigation").hover(
	  function () {
		$('.sub').addClass("side-active");
		$('.s-calles').addClass("s-calles-nav");
		$('ul.l-calles').addClass("l-calles-nav");
	  },
	  function () {
		$('.sub').removeClass("side-active");
		$('.s-calles').removeClass("s-calles-nav");
		$('ul.l-calles').removeClass("l-calles-nav");
	  }
	);	
	
	
//MOVER EL DETALLE DE REPORTE AL HACER HOVER EN LA BARRA AZUL
	$(".first-navigation").hover(
	  function () {
		$('.report').addClass("report-medium");
	  },
	  function () {
		$('.report').removeClass("report-medium");
	  }
	);
	
//TOOLTIPS
	$('.tool').tooltip("show")
	$('.error').tooltip({		
		template: '<div class="tooltip tooltip-error" role="tooltip"><div class="tooltip-arrow tooltip-arrow-red"></div><div class="tooltip-inner"></div></div>',
	})
	
	
//LOCAL SCROLL EN COMO FUNCIONA
	 $('#q-links').localScroll({
			target:'#answers'
	 });