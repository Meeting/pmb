<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <title>Por mi Barrio</title>

    <!--CSS-->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/main.css" rel="stylesheet" />
    <link href="css/sprites.css" rel="stylesheet">
    <link href="css/bootstrap-combined.min.css" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-select.css">
    
    <!--FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>


<body>
<?php include ('inc/sprites.php'); ?>
	<!--MAIN CONTAINER-->
    <div class="container">
    	<!--FIRST LEVEL NAVIGATION-->
        <nav class="first-navigation">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img class="img-responsive" src="images/bg/PMB-small.png" alt=""></a>
            <ul class="r-small">
                <li class="reportar reportar-small"><a href="reportar.php"><svg class="icon icon-icono8" viewBox="0 0 24 24"><use xlink:href="#icon-icono8"></use></svg><span>Reportar</span></a></li>
            </ul>
            <div class="navbar-collapse collapse">
                <ul>
                    <li class="home"><a href="index.php"><span></span></a></li>
                    <li class="reportar"><a href="reportar.php"><svg class="icon icon-icono8" viewBox="0 0 24 24"><use xlink:href="#icon-icono8"></use></svg> <span>Reportar</span></a></li>
                    <li class="reportes"><a href="lista-reportes.php"><svg class="icon icon-icono1" viewBox="0 0 24 24"><use xlink:href="#icon-icono1"></use></svg> <span>Reportes</span></a></li>
                    <li class="help"><a href="info-help.php"><svg class="icon icon-icono3" viewBox="0 0 24 24"><use xlink:href="#icon-icono3"></use></svg> <span>Ayuda</span></a></li>
                    <li class="profile"><a href="profile.php"><img class="img-circle" width="35" height="35" src="images/user-profile-ref.png"/><span>Perfil</span></a></li>
                </ul>
                <ul class="help-info">
                    <!-- <li class="doc-info"><a href="javascript:;">Doc info</a></li> -->
                    <li class="data"><a href="javascript:;">DATA</a></li>
                    <li class="my-society"><a href="javascript:;">My Society</a></li>
                </ul>
            </div>
        </nav>
        
        
        <!--COMO FUNCIONA-->
 		<div class="reportes sub report-list" id="report-list">
            <div class="c-scroll">
                <div class="rl scrolled">
                	<div class="como-funciona" id="q-links">
                    	<h2>Información</h2>
                        <dl>
                        	<dt>Como funciona</dt>
                            <dd>· <a href="#faq01">¿Cómo hago un reporte?</a></dd>
                            <dd>· <a href="#faq02">¿Necesito Facebook para registrarme?</a></dd>
                            <dd>· <a href="#faq03">¿Cómo comento en un reporte?</a></dd>
                            <dd>· <a href="javascript:;">¿Cómo hago un reporte?</a></dd>
                        </dl>
                        <dl>
                        	<dt>Acerca de por mi barrio</dt>
                            <dd>· <a href="javascript:;">¿Cómo hago un reporte?</a></dd>
                            <dd>· <a href="javascript:;">¿Necesito Facebook para registrarme?</a></dd>
                            <dd>· <a href="javascript:;">¿Cómo comento en un reporte?</a></dd>
                            <dd>· <a href="javascript:;">¿Cómo hago un reporte?</a></dd>
                        </dl>
                        <dl>
                        	<dt>Acerca de por mi barrio</dt>
                            <dd>· <a href="javascript:;">¿Cómo hago un reporte?</a></dd>
                            <dd>· <a href="javascript:;">¿Necesito Facebook para registrarme?</a></dd>
                            <dd>· <a href="javascript:;">¿Cómo comento en un reporte?</a></dd>
                            <dd>· <a href="javascript:;">¿Cómo hago un reporte?</a></dd>
                        </dl>
                        <dl>
                        	<dt>Acerca de por mi barrio</dt>
                            <dd>· ¿Qué es Por Mi Barrio?</dd>
                            <dd>· ¿Cómo funciona con la IM?</dd>
                            <dd>· ¿De dónde surgió la idea?</dd>
                            <dd>· etc</dd>                        
                        </dl>
                        <dl>
                        	<dt>Acerca de por mi barrio</dt>
                            <dd>· ¿Qué es Por Mi Barrio?</dd>
                            <dd>· ¿Cómo funciona con la IM?</dd>
                            <dd>· ¿De dónde surgió la idea?</dd>
                            <dd>· etc</dd>                        
                        </dl>
                        <dl>
                        	<dt>Acerca de por mi barrio</dt>
                            <dd>· ¿Qué es Por Mi Barrio?</dd>
                            <dd>· ¿Cómo funciona con la IM?</dd>
                            <dd>· ¿De dónde surgió la idea?</dd>
                            <dd>· etc</dd>                        
                        </dl>
                        <dl>
                        	<dt>Acerca de por mi barrio final item</dt>
                            <dd>· ¿Qué es Por Mi Barrio?</dd>
                            <dd>· ¿Cómo funciona con la IM?</dd>
                            <dd>· ¿De dónde surgió la idea?</dd>
                            <dd>· etc</dd>                        
                        </dl>
                    </div>
                </div>
            </div>
        </div>
        <!--end como funcional-->
        
        
        
        <!--COMO FUNCIONA-->
 		<div class="reportes c-respuestas">
            <div class="c-scroll">
                <div class="rl scrolled" id="answers">
                    <!--Respuesta-->
                    <div class="respuesta" id="faq01">
                        <h2>¿Qué es Por Mi Barrio?</h2>                            
                        <p>Una plataforma que permitirá a las personas que viven en Montevideo enviar reportes sobre daños, 
                        desperfectos, vandalismo y otros problemas de nuestra ciudad desde su computadora o celular.</p>
                        <p>Es decir, cuando encuentres un problema en tu barrio (un pozo en la calle, problemas de arbolado o de iluminación, 
                        contenedores de basura rotos, etc.) vas a poder entrar en pormibarrio.uy desde tu computadora (ceibalitas incluidas) 
                        o celular y reportarlo. Para hacer esto, sólo tenés que localizar el problema en un mapa. También se pueden subir fotos y comentarios.</p>
                        <p>Cuando vuelvas a entrar a la página web vas a poder ver tu reporte marcado en un mapa, así como los de los/as demás 
                        usuarios/as. También podrás ver los reportes que la <a href="javascript:;">Intendencia de Montevideo (IM)</a> marcó como solucionados y 
                        los que todavía son problemas para el barrio. </p>                  
                    </div>
                    <!--end respuesta-->
                    <!--Respuesta-->
                    <div class="respuesta" id="faq02">
                        <h2>¿Cómo hago un reporte?</h2>                            
                        <p>Una plataforma que permitirá a las personas que viven en Montevideo enviar reportes sobre daños, 
                        desperfectos, vandalismo y otros problemas de nuestra ciudad desde su computadora o celular.</p>
                        <p>Es decir, cuando encuentres un problema en tu barrio (un pozo en la calle, problemas de arbolado o de iluminación, 
                        contenedores de basura rotos, etc.) vas a poder entrar en pormibarrio.uy desde tu computadora (ceibalitas incluidas) 
                        o celular y reportarlo. Para hacer esto, sólo tenés que localizar el problema en un mapa. También se pueden subir fotos y comentarios.</p>
                        <p>Cuando vuelvas a entrar a la página web vas a poder ver tu reporte marcado en un mapa, así como los de los/as demás 
                        usuarios/as. También podrás ver los reportes que la <a href="javascript:;">Intendencia de Montevideo (IM)</a> marcó como solucionados y 
                        los que todavía son problemas para el barrio. </p>                  
                    </div>
                    <!--end respuesta-->
                    <!--Respuesta-->
                    <div class="respuesta" id="faq03">
                        <h2>¿Necesito Facebook para registrarme?</h2>                            
                        <p>Una plataforma que permitirá a las personas que viven en Montevideo enviar reportes sobre daños, 
                        desperfectos, vandalismo y otros problemas de nuestra ciudad desde su computadora o celular.</p>
                        <p>Es decir, cuando encuentres un problema en tu barrio (un pozo en la calle, problemas de arbolado o de iluminación, 
                        contenedores de basura rotos, etc.) vas a poder entrar en pormibarrio.uy desde tu computadora (ceibalitas incluidas) 
                        o celular y reportarlo. Para hacer esto, sólo tenés que localizar el problema en un mapa. También se pueden subir fotos y comentarios.</p>
                        <p>Cuando vuelvas a entrar a la página web vas a poder ver tu reporte marcado en un mapa, así como los de los/as demás 
                        usuarios/as. También podrás ver los reportes que la <a href="javascript:;">Intendencia de Montevideo (IM)</a> marcó como solucionados y 
                        los que todavía son problemas para el barrio. </p>                  
                    </div>
                    <!--end respuesta-->
                    
                    
                    
                </div>
            </div>
        </div>
        <!--end como funcional-->
        
        
        
        
        
        
        
        
        
        <!--MAP-->
        <div class="map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3271.959063955584!2d-56.186689099999995!3d-34.9074768!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x959f81cb87dc458b%3A0x2da7d92a1efcbe7b!2sEjido!5e0!3m2!1ses!2suy!4v1401075267114" width="100%" height="100%" frameborder="0" style="border:0"></iframe>        </div>
        <!--end map-->
    </div>
    <!--/main container-->

	<!--SCRIPTS-->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
	<script src="js/jquery.slimscroll.js"></script>
    <script src="js/jquery.responsiveText.js"></script>
    <script src="js/actions.js"></script>
    <script src="js/jquery.localScroll.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    
	<script src="js/main.js"></script>
    <script src="js/actions.js"></script>
    <script src="js/bootstrap-select.js"></script>

	<script type="text/javascript">
      window.onload = function () {
        $('.selectpicker').selectpicker();
      };
    </script>

    
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5065c0290cc90532"></script>
    
</body>
</html>
