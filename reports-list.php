        <div class="reportes sub report-list" id="report-list">
            <!--Intro reports-->
            <div class="ir" id="top-reports">
                <div class="c-t"><!--nuevo-->
                    <h2>Todos los reportes</h2>
                    <p>Donec quis turpis et elit vehicula dignissim ut quis lectus. Aenean gravida nisi vitae enim dignissim imperdiet. 
                    Duis varius justo purus, et lobortis turpis sagittis sed.</p>
                </div>
            </div>
            <!--/end intro reports-->
            <!--REPORTS LIST-->
            <div class="c-scroll"><!--nuevo-->
                <div class="rl scrolled">                    	
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-arbolado"></div>
                        <div class="it-r-info">
                            <h3 class="arbolado">Arbolado</h3>
                            <p>Árboles o ramas caídas: accidental</p>
                            <ul class="details">
                                <li>Hoy, 16:00hs</li>
                                <li class="icon-camera"><span></span></li>
                                <li class="icon-comments"><span></span></li>
                                <li class="icon-tags"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-alcantarillas"></div>
                        <div class="it-r-info">
                            <h3 class="alcantarillas">alcantarillas y registros</h3>
                            <p>Alcantarilla obstruida</p>
                            <ul class="details">
                                <li>Hoy, 19:00hs</li>
                                <li class="icon-camera"><span></span></li>
                                <li class="icon-comments"><span></span></li>
                                <li class="icon-tags"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-basura"></div>
                        <div class="it-r-info">
                            <h3 class="basura">Basura, Escombros y ferias</h3>
                            <p>Levante de bolsas de barrido</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-desarrollo"></div>
                        <div class="it-r-info">
                            <h3 class="desarrollo">Desarrollo</h3>
                            <p>Problema de desarrollo 2</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-barometrica"></div>
                        <div class="it-r-info">
                            <h3 class="barometrica">Barometrica</h3>
                            <p>Solicitud de barométrica</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Reporte final</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                </div>
                <!--/end reports list-->
            </div>
        </div>
        <!--end listado de reportes-->
