<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Por mi Barrio</title>

    <!--CSS-->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/main.css" rel="stylesheet" />
    <link href="css/sprites.css" rel="stylesheet">
    <link href="css/bootstrap-combined.min.css" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap-select.css">
    
    <!--FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body>
<?php include ('inc/sprites.php'); ?>
	<!--MAIN CONTAINER-->
    <div class="container">
    	<!--TOP CONTAINER-->
        <div class="top-container">
        	<!--BUSCAR CALLES-->
            <div id="s-calles" class="s-calles">
            	<form role="form" class="form-inline">
                    <div class="form-group pull-left">
                    	<input class="form-control" placeholder="Ir a"/>
                    </div>
                    <div class="form-group pull-right">
                    	<button type="submit" class="btn btn-search pull-right">Buscar</button>
                    </div>
                </form>
            </div>
            
            <!--lista CALLES-->
            <ul class="list-unstyled l-calles" style="display:none;">
              <li><span>#1</span>  Parque Batlle, Av.Ricaldoni 1234</li>
              <li><span>#2</span>  General Artigas, Luis Batlle Berres</li>
              <li><span>#3</span>  General Artigas, Luis Batlle Berres</li>
            </ul>
            <!--/lista CALLES-->
        
        
        	<!--USER INFO-->
            <div class="info-user" id="info-user">
            	<!--Vista sin login-->
                <ul class="list-inline user-login" style="display:none;">
                	<li><a href="javascript:;">Inicia sesión</a> ó <a class="green" href="javascript:;">Registrate</a></li>
                </ul>
                <!-- /Vista sin login-->
                <!-- Vista logueado -->
                <div class="dropdown user-log-on" style="display:block">
                    <a data-toggle="dropdown" class="user-name" href="#"><img class="img-circle" src="images/user-profile-ref.png" 
                    alt="Martin Garrido">Martín Garrido garrido nombre largo</a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                        <li><a class="user-reportes" href="lista-reportes.php"><svg class="icon icon-icono1" viewBox="0 0 24 24"><use xlink:href="#icon-icono1"></use></svg> <p>Mis reportes</p></a></li>
                        <li><a class="user-profile" href="editar-perfil.php"><svg class="icon icon-icono4" viewBox="0 0 24 24"><use xlink:href="#icon-icono4"></use></svg> Editar mi perfil</a></li>
                        <li><a class="user-sesion" href="javascript:;"><svg class="icon icon-icono2" viewBox="0 0 24 24"><use xlink:href="#icon-icono2"></use></svg> Cerrar Sesión</a></li>
                    </ul>
                </div>
                 <!-- /Vista logueado-->
            </div>
            <!--.end info user-->
        </div>        
        <!--.end top container--> 
    	<!--FIRST LEVEL NAVIGATION-->
        <nav class="first-navigation">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img class="img-responsive" src="images/bg/PMB-small.png" alt=""></a>
            <ul class="r-small">
                <li class="reportar reportar-small"><a href="reportar.php"><svg class="icon icon-icono8" viewBox="0 0 24 24"><use xlink:href="#icon-icono8"></use></svg><span>Reportar</span></a></li>
            </ul>
            <div class="navbar-collapse collapse">
                <ul>
                    <li class="home"><a href="index.php"><span></span></a></li>
                    <li class="reportar"><a href="javascript:void(0);" id="reportar"><svg class="icon icon-icono8" viewBox="0 0 24 24"><use xlink:href="#icon-icono8"></use></svg> <span>Reportar</span></a></li>
                    <li class="reportes"><a href="javascript:void(0);" id="reportes"><svg class="icon icon-icono1" viewBox="0 0 24 24"><use xlink:href="#icon-icono1"></use></svg> <span>Reportes</span></a></li>
                    <li class="help"><a href="info-help.php"><svg class="icon icon-icono3" viewBox="0 0 24 24"><use xlink:href="#icon-icono3"></use></svg> <span>Ayuda</span></a></li>
                    <!--<li class="profile"><a href="profile.php"><img class="img-circle" width="35" height="35" src="images/user-profile-ref.png"/><span>Perfil</span></a></li>-->
                </ul>
                <ul class="help-info">
                    <!-- <li class="doc-info"><a href="javascript:;">Doc info</a></li> -->
                    <li class="data"><a href="javascript:;">DATA</a></li>
                    <li class="my-society"><a href="javascript:;">My Society</a></li>
                </ul>
            </div>
        </nav>
        
        
        <!--REPORT LIST-->
 <div class="reportes sub report-list mostrar" id="report-list">
            <!--Intro reports-->
            <div class="ir" id="top-reports">
                <div class="c-t"><!--nuevo-->
                    <h2>Todos los reportes</h2>
                    <p>Donec quis turpis et elit vehicula dignissim ut quis lectus. Aenean gravida nisi vitae enim dignissim imperdiet. 
                    Duis varius justo purus, et lobortis turpis sagittis sed.</p>
                </div>
            </div>
            <!--/end intro reports-->
            <!--REPORTS LIST-->
            <div class="c-scroll"><!--nuevo-->
                <div class="rl scrolled">                    	
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-arbolado"></div>
                        <a href="report-detail.php">
                        <div class="it-r-info">
                            <h3 class="arbolado">Arbolado</h3>
                            <p>Árboles o ramas caídas: accidental</p>
                            <ul class="details">
                                <li>Hoy, 16:00hs</li>
                                <li class="icon-camera"><span></span></li>
                                <li class="icon-comments"><span></span></li>
                                <li class="icon-tags"><span></span></li>
                            </ul>                                                       
                        </div>
                        </a> 
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-alcantarillas"></div>
                        <div class="it-r-info">
                            <h3 class="alcantarillas">alcantarillas y registros</h3>
                            <p>Alcantarilla obstruida</p>
                            <ul class="details">
                                <li>Hoy, 19:00hs</li>
                                <li class="icon-camera"><span></span></li>
                                <li class="icon-comments"><span></span></li>
                                <li class="icon-tags"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-basura"></div>
                        <div class="it-r-info">
                            <h3 class="basura">Basura, Escombros y ferias</h3>
                            <p>Levante de bolsas de barrido</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-desarrollo"></div>
                        <div class="it-r-info">
                            <h3 class="desarrollo">Desarrollo</h3>
                            <p>Problema de desarrollo 2</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-barometrica"></div>
                        <div class="it-r-info">
                            <h3 class="barometrica">Barometrica</h3>
                            <p>Solicitud de barométrica</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Corte vereda para trabajos</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                    <!--REPORT-->                        
                    <div class="it-r">
                        <div class="r-icon r-vereda"></div>
                        <div class="it-r-info">
                            <h3 class="vereda">Vereda</h3>
                            <p>Reporte final</p>
                            <ul class="details">
                                <li>Hoy, 08:00hs</li>
                                <li class="icon-camera"><span></span></li>
                            </ul>                            
                        </div>
                    </div>
                    <!--/end report-->
                </div>
                <!--/end reports list-->
            </div>
        </div>
        <!--end listado de reportes-->
        
        
        <!--CREATE REPORT-->
        <div class="reportes new-report sub" id="add-report">
		<div class="c-scroll-reportar"><!--nuevo-->
            <div class="scrolled-reportar">
            <div class="c-form-report">
                <div class="ir">
                    <h2>Realizar un reporte</h2>
                    <p>Donec quis turpis et elit vehicula dignissim ut quis lectus. Aenean gravida nisi vitae enim dignissim imperdiet. 
                    Duis varius justo purus, et lobortis turpis sagittis sed.</p>
                </div>
                <form role="form">
                  <!--<div class="form-group">
                    <label class="active" for="direccion">Dirección del problema</label>
                    <input class="form-control" type="text" id="direccion" placeholder="Calle">
                    <input class="form-control" type="text" placeholder="Esquina/Número">
                  </div>-->                          
                    <!--Category-->
                    <div class="form-group">
                        <label class="active" for="categoria">Categoría & subcategoría</label>
                        <select id="categoria" class="selectpicker form-control">
                        	<option>-- Selecciona un grupo --</option>
                        	<option data-icon="ico-heart" class="alcantarillas">Alcantarillas y registros</option>
                            <option data-icon="ico-glass" class="arbolado">Arbolado</option>
                            <option data-icon="ico-film" class="basura">Basura, Escombros y ferias</option>
                            <!-- <option data-icon="ico-home" class="desarrollo">Desarrollo</option> -->
                            <option data-icon="ico-print" class="barometrica">Barométrica</option>
                            <!-- <option data-icon="ico-print" class="vereda">Vereda</option> -->
                        </select>
                        <!--Subcategory-->
                        <select id="categoria" class="selectpicker form-control">
                            <option>Alcantarilla obstruida</option>
                            <option>Alcantarilla rota</option>
                            <option>Registro nuevo</option>
                            <option>Registro roto</option>
                        </select>
                    </div>
                    <!--Description-->
                    <div class="form-group">
                        <label for="descripcion">Descripción</label>
                        <textarea class="form-control" id="descripcion" rows="4"></textarea>
                    </div>
                    <!--Images-->
                    <div class="form-group">
                        <label for="foto">Agregar una foto</label>
                        <div class="drag">
                            <p class="InputButton">Clic para explorar</p>
                            <input type="file" id="InputFile">
                        </div>
                        <p class="help-block">* El archivo no puede pesar más de 2MB (JPG, PNG, GIF)</span>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-reportar btn-disabled" disabled="disabled">Publicar reporte</button>
                        <!--Cuando el usuario inicia sesion, remover la clase btn-disabled-->
                    </div>
                </form>
            </div>
            <!--INICIAR SESION-->
            <div class="login">
                <div class="bloque-sesion">
                    <div class="ir">
                        <h2>Accede a tu cuenta</h2>
                        <p>Conectate a tu cuenta para poder finalizar tu reporte y enviarlo a la Intendencia.</p>
                    </div>
                    <div class="social-buttons">
                        <button class="btn btn-block btn-social btn-facebook">Registrate con Facebook</button>
                        <button class="btn btn-block btn-social btn-twitter">Registrate con Twitter</button>
                    	<p>...ó ingresa los datos si ya tienes cuenta:</p>
                    </div>
                    <form role="form">
                        <div class="form-group">
                            <input class="form-control error" type="text" placeholder="Correo electrónico">                            	
                            <input class="form-control" type="password" placeholder="Contraseña">                            	
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-reportar">Iniciar sesión</button>
                        </div>
                        <p class="no-account">¿No utilizas redes sociales? No hay problema!
                            <br />                        
                            <a class="green registrate" href="javascript:;">Regístrate aquí con tu mail</a>
                        </p>
                        <!--<div class="form-group">
                            <a href="javascript:;" class="btn btn-block btn-green registrate">Regístrate aquí</a>
                        </div>-->
                    </form>
                </div>
                <!--end iniciar sesion-->
                <!--LOGIN-->
                <div class="bloque-registro">
                    <div class="ir">
                        <h2>Regístrate y se parte</h2>
                        <p>Crea una cuenta en PorMiBarrio y comienza a colaborar.</p>
                    </div>
                    <form role="form">
                        <div class="form-group mas-padding">
                            <input class="form-control" type="text" placeholder="Nombre y apellido">                            	
                            <input class="form-control" type="text" placeholder="Cédula de Identidad">                            	
                            <input class="form-control" type="email" placeholder="Correo Electrónico" >
                            <input class="form-control" type="password" placeholder="Contraseña" >
                        </div> 
                        <div class="checkbox">
                            <label>
                              <input type="checkbox"> Al continuar con tu registro, aceptas haber leído y aceptado nuestra 
                              <a href="javascript:;"> Política de privacidad</a>.
                            </label>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-green">Regístrate</button>
                        </div>
                      </form>
                </div>
            </div>
            </div>                
            <!--end login-->
            </div>                
        </div>
        <!--end reportar e iniciar sesion-->
        
        
        
        <!--MAP-->
        <div class="pin-content">
        	<div><img src="images/pins/pin-arbolado.png"/></div>
        </div>
        <div class="map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3271.959063955584!2d-56.186689099999995!3d-34.9074768!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x959f81cb87dc458b%3A0x2da7d92a1efcbe7b!2sEjido!5e0!3m2!1ses!2suy!4v1401075267114" width="100%" height="100%" frameborder="0" style="border:0"></iframe>        </div>
        <!--end map-->
    </div>
    <!--/main container-->

    <!--SCRIPTS-->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
	<script src="js/jquery.slimscroll.js"></script>
    <script src="js/jquery.responsiveText.js"></script>
    <script src="js/actions.js"></script>
    <script src="js/jquery.localScroll.js"></script>
    <script src="js/jquery.scrollTo.min.js"></script>
    
	<script src="js/main.js"></script>
    <script src="js/actions.js"></script>
    <script src="js/bootstrap-select.js"></script>

	<script type="text/javascript">
      window.onload = function () {
        $('.selectpicker').selectpicker();
      };
    </script>

    
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5065c0290cc90532"></script>
    
</body>
</html>
